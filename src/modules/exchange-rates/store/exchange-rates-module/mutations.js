const SET_BASE_TOKENS = (state, data) => {
  state.baseTokens = data;
};

const SET_BASE_TOKEN_OPTIONS = (state, data) => {
  state.baseTokenOptions = data;
  state.selectedTokenB = {
    token: null,
    value: 0,
    quote: 0
  };
  state.selectedTokenC = {
    token: null,
    value: 0,
    quote: 0
  };
};

const ENABLE_TOKEN_C = (state) => {
  state.isTokenCActive = true;
};

const DISABLE_TOKEN_C = (state) => {
  state.isTokenCActive = false;
  state.selectedTokenC = {
    token: null,
    value: 0,
    quote: 0
  };
};

const SWAP_TOKENS = (state) => {
  let auxToken = state.selectedTokenA
  state.selectedTokenA = state.selectedTokenB;
  state.selectedTokenB = auxToken;
};

const SET_TOKEN = (state, data) => {
  let token = {
    a: () => {
      state.selectedTokenA.token = data.token;
      return true;
    },
    b: () => {
      state.selectedTokenB.token = data.token;
      return true;
    },
    c: () => {
      state.selectedTokenC.token = data.token;
      return true;
    },
    default: () => {
      console.log('position not found');
      return true;
    },
  }
  token[data.position]() || token['default']()
};

const SET_VALUE = (state, data) => {
  let value = {
    a: () => {
      state.selectedTokenA.value = data.value;
      return true;
    },
    b: () => {
      state.selectedTokenB.value = data.value;
      return true;
    },
    c: () => {
      state.selectedTokenC.value = data.value;
      return true;
    },
    default: () => {
      console.log('position not found');
      return true;
    },
  }
  value[data.position]() || value['default']()
};

const SET_TOKEN_PRICE = (state, data) => {
  let price = {
    a: () => {
      state.selectedTokenA.value = data.price;
      return true;
    },
    b: () => {
      state.selectedTokenB.value = data.price;
      state.selectedTokenB.quote = data.quote;
      return true;
    },
    c: () => {
      state.selectedTokenC.value = data.price;
      state.selectedTokenC.quote = data.quote;
      return true;
    },
    default: () => {
      console.log('position not found');
      return true;
    },
  }
  price[data.position]() || price['default']()
};

const SET_CURRENCIES_BY_COUNTRY = (state, data) => {
  state.currenciesByCountry = data.currencies;
};

const SET_SELECTED_CURRENCY = (state, data) => {
  state.selectedCurrency = data;
};

const SET_EXCHANGE_RATES = (state, data) => {
  state.exchangeRates.stables = data.stables;
  state.exchangeRates.cryptos = data.cryptos;
};

export default {
  SET_BASE_TOKENS,
  SET_BASE_TOKEN_OPTIONS,
  ENABLE_TOKEN_C,
  DISABLE_TOKEN_C,
  SWAP_TOKENS,
  SET_TOKEN,
  SET_VALUE,
  SET_TOKEN_PRICE,
  SET_CURRENCIES_BY_COUNTRY,
  SET_SELECTED_CURRENCY,
  SET_EXCHANGE_RATES
};
