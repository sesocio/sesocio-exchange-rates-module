import actions from "./actions";
import mutations from "./mutations";

const state = {
  baseTokens: [],
  baseTokenOptions: [],
  selectedTokenA: {
    token: null,
    value: 0,
    quote: 0
  },
  selectedTokenB: {
    token: null,
    value: 0,
    quote: 0
  },
  isTokenCActive: false,
  selectedTokenC: {
    token: null,
    value: 0,
    quote: 0
  },
  selectedCurrency: "",
  currenciesByCountry: [],
  exchangeRates: {
    stables: [],
    cryptos: [],
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};