import Axios from 'axios';

const getBaseTokens = async ({ commit }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/currencies/list_of_quotation`;

      const response = await Axios.get(url)
      commit('SET_BASE_TOKENS', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getBaseTokenOptions = async ({ commit, state }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/currencies/list_of_quotation_options?currency=${state.selectedTokenA.token.currency}`;

      const response = await Axios.get(url)
      commit('SET_BASE_TOKEN_OPTIONS', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const enableTokenC = async ({ commit }) => {
  commit('ENABLE_TOKEN_C')
};

const disableTokenC = async ({ commit }) => {
  commit('DISABLE_TOKEN_C')
};

const swapTokens = async ({ commit }) => {
  commit('SWAP_TOKENS')
};

const setToken = async ({ commit }, data) => {
  commit('SET_TOKEN', {
    'token': data.token,
    'position': data.position
  })
};

const setValue = async ({ commit }, data) => {
  commit('SET_VALUE', {
    'value': data.value,
    'position': data.position
  })
};

const convertValueToken = async ({ state, commit, rootState }, data) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/currencies/convert_from_to`;

      const response = await Axios.get(url, {
        params: {
          'from': data.fromItem.token.currency,
          'to': data.toItem.token.currency,
          'country': rootState.core.country,
          'amount': data.amount,
          'direction': data.direction,
        }
      })
      commit('SET_TOKEN_PRICE', {
        'price': response.data.price,
        'quote': response.data.quote,
        'position': data.toPosition,
        'direction': data.direction
      })

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const runConvertValueToken = async ({ state, dispatch, rootState }, position) => {
  let positions = {
    // selectedTokenA
    'a': async () => {
      if (state.selectedTokenA.token && state.selectedTokenB.token) {
        await dispatch("convertValueToken", {
          'fromItem': state.selectedTokenA,
          'toItem': state.selectedTokenB,
          'toPosition': 'b',
          'amount': state.selectedTokenA.value,
          'direction': 1
        })
        .then(response => {})
        .catch(e => {
          console.log(e);
        })
      }
      if (state.selectedTokenA.token && state.selectedTokenC.token) {
        await dispatch("convertValueToken", {
          'fromItem': state.selectedTokenA,
          'toItem': state.selectedTokenC,
          'toPosition': 'c',
          'amount': state.selectedTokenA.value,
          'direction': 1
        })
        .then(response => {})
        .catch(e => {
          console.log(e);
        })
      }
      return true;
    },
    // selectedTokenB
    'b': async () => {
      if (state.selectedTokenA.token && state.selectedTokenB.token) {
        await dispatch("convertValueToken", {
          'fromItem': state.selectedTokenA,
          'toItem': state.selectedTokenB,
          'toPosition': 'a',
          'amount': state.selectedTokenB.value,
          'direction': 0
        })
        .then(response => {})
        .catch(e => {
          console.log(e);
        })
      }
      if (state.selectedTokenA.token && state.selectedTokenC.token) {
        await dispatch("convertValueToken", {
          'fromItem': state.selectedTokenA,
          'toItem': state.selectedTokenC,
          'toPosition': 'c',
          'amount': state.selectedTokenA.value,
          'direction': 1
        })
        .then(response => {})
        .catch(e => {
          console.log(e);
        })
      }
      return true;
    },
    // selectedTokenC
    'c': async () => {
      if (state.selectedTokenA.token && state.selectedTokenC.token) {
        await dispatch("convertValueToken", {
          'fromItem': state.selectedTokenA,
          'toItem': state.selectedTokenC,
          'toPosition': 'a',
          'amount': state.selectedTokenC.value,
          'direction': 0
        })
        .then(response => {})
        .catch(e => {
          console.log(e);
        })
      }
      if (state.selectedTokenA.token && state.selectedTokenB.token) {
        await dispatch("convertValueToken", {
          'fromItem': state.selectedTokenA,
          'toItem': state.selectedTokenB,
          'toPosition': 'b',
          'amount': state.selectedTokenA.value,
          'direction': 1
        })
        .then(response => {})
        .catch(e => {
          console.log(e);
        })
      }
      return true;
    },
    // none
    'default': () => {
      console.log('position not found');
    }
  };
  positions[position]() || positions['default']();
};

const getCurrenciesByCountry = async ({ commit, rootState }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/home/show_currencies_by_country`;

      const response = await Axios.get(url, {
        "params": {
          "country": rootState.core.country.toLowerCase()
        }
      })
      commit('SET_CURRENCIES_BY_COUNTRY', response.data)
      commit('SET_SELECTED_CURRENCY', response.data.currencies[0])

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getExchangeRates = async ({ commit, rootState }, data) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/exchange_rate/${rootState.core.country.toLowerCase()}`;

      const response = await Axios.get(url, {
        "params": {
          "currency": data.currency
        }
      })
      commit('SET_EXCHANGE_RATES', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const setSelectedCurrency = ({ commit }, data) => {
  commit('SET_SELECTED_CURRENCY', data)
};

export default {
  getBaseTokens,
  getBaseTokenOptions,
  enableTokenC,
  disableTokenC,
  swapTokens,
  setToken,
  setValue,
  convertValueToken,
  runConvertValueToken,
  getExchangeRates,
  setSelectedCurrency,
  getCurrenciesByCountry
};
