export default [
    {
      path: "/exchange_rates",
      name: "exchange_rates",
      component: () => import(/* webpackChunkName: "ExchangeRates" */ '../pages/index'),
      title: "Exchange Rates"
    }
  ];
  